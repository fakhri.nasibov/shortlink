package com.nasibov.shortlink.configs;

import com.nasibov.shortlink.services.link.LinkService;
import com.nasibov.shortlink.services.profile.ProfileService;
import com.nasibov.shortlink.services.user.UserService;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;

@TestConfiguration
public class TestServicesConfig {

    @MockBean
    UserService userService;

    @MockBean
    ProfileService profileService;

    @MockBean
    LinkService linkService;
}
