package com.nasibov.shortlink.repositories;

import com.nasibov.shortlink.models.Link;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LinkRepositoryTest {
    @Autowired
    LinkRepository linkRepository;

    private ArrayList<Link> links = new ArrayList<>();
    private String username = "username";

    @Before
    public void setUp() throws Exception {

        for (int i = 0; i < 5; i++) {
            Link link = new Link();
            String baseLink = "baseLink" + i;
            link.setOwnerUsername(this.username);
            link.setBaseLink(baseLink);
            this.links.add(link);
        }

        this.linkRepository.saveAll(this.links);
    }

    @Test
    public void findAllByOwnerUsernameTest() {
        List<Link> links = this.linkRepository.findAllByOwnerUsername(username);
        assertEquals(links, this.links);
    }
}