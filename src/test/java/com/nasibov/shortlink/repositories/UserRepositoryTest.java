package com.nasibov.shortlink.repositories;

import com.nasibov.shortlink.models.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    private User user;

    @Before
    public void setUp() {
        User user = new User();
        user.setUsername("username");
        user.setPassword("password");
        user.setFirstname("name");
        this.user = user;
        userRepository.save(this.user);
    }

    @Test
    public void findUserByUsernameTest() {
        Optional<User> user = userRepository.findByUsername(this.user.getUsername());
        user.ifPresent(u -> {
            Assert.assertEquals(this.user.getUsername(), u.getUsername());
            Assert.assertNotNull(u.getId());
        });
    }
}
