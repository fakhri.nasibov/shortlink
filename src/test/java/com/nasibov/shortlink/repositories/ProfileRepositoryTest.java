package com.nasibov.shortlink.repositories;

import com.nasibov.shortlink.models.Profile;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProfileRepositoryTest {
    @Autowired
    ProfileRepository profileRepository;

    private Profile profile;

    @Before
    public void setUp() {
        Profile profile = new Profile();
        profile.setUsername("username");
        profile.setPhotoUrl("url");
        this.profile = profile;
        profileRepository.save(profile);
    }

    @Test
    public void findProfileByUserIdTest() {
        Optional<Profile> profile = profileRepository.findByUsername(this.profile.getUsername());
        profile.ifPresent(p -> {
            Assert.assertEquals(this.profile.getUsername(), p.getUsername());
            Assert.assertNotNull(p.getId());
        });
    }
}
