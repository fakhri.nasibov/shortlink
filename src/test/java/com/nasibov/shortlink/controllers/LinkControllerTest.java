package com.nasibov.shortlink.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nasibov.shortlink.configs.TestServicesConfig;
import com.nasibov.shortlink.models.Link;
import com.nasibov.shortlink.services.link.LinkService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@WebMvcTest(LinkController.class)
@Import(TestServicesConfig.class)
public class LinkControllerTest {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    LinkService linkService;

    @Autowired
    MockMvc mockMvc;

    private List<Link> links;
    private Link link;

    @Before
    public void setUp() {
        this.links = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Link link = new Link();
            String baseLink = "baseLink" + i;
            link.setOwnerUsername("test");
            link.setBaseLink(baseLink);
            this.links.add(link);
        }
        this.link = new Link();
        link.setOwnerUsername("test");
        link.setBaseLink("baseLink");
    }

    @Test
    @WithMockUser("test")
    public void getLinkListForUser() throws Exception {

        when(linkService.getAllByUsername(any(String.class))).thenReturn(this.links);
        this.mockMvc
                .perform(get("/links", "test"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(5)));
        verify(linkService, times(1)).getAllByUsername(any(String.class));
    }

    @Test
    @WithMockUser("test")
    public void saveNewLinkTest() throws Exception {
        when(linkService.saveNewLink(any(String.class), any(String.class))).thenReturn(this.link);
        this.mockMvc.perform(post("/links").param("url", "url"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.ownerUsername", is("test")));
        verify(linkService, times(1)).saveNewLink(any(String.class), any(String.class));
    }

    @Test
    @WithMockUser("test")
    public void saveNewLinkEmptyUrl() throws Exception {
        this.mockMvc.perform(post("/links"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void redirectToBaseUrlByLinkIdTest() throws Exception {
        when(linkService.getLinkById(any(String.class))).thenReturn(this.link);
        this.mockMvc.perform(get("/links/{id}", "linkId"))
        .andExpect(status().is3xxRedirection());
    }
}