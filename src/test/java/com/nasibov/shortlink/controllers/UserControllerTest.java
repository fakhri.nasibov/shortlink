package com.nasibov.shortlink.controllers;

import com.nasibov.shortlink.configs.TestServicesConfig;
import com.nasibov.shortlink.models.Profile;
import com.nasibov.shortlink.services.profile.ProfileService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
@Import(TestServicesConfig.class)
public class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ProfileService profileService;

    @Test
    @WithMockUser(value = "test")
    public void getProfilePageTest() throws Exception {
        Profile profile = new Profile();
        profile.setUsername("test");
        Mockito.when(profileService.findByUsername(any(String.class))).thenReturn(profile);
        this.mockMvc.perform(get("/users/{username}/profile", "test"))
                .andExpect(status().isOk())
                .andExpect(view().name("profile"))
                .andExpect(model().attribute("profile", notNullValue()))
                .andExpect(model().attribute("links", notNullValue()))
                .andExpect(model().size(2));
    }

    @Test
    @WithMockUser(value = "test")
    public void getProfilePageForOtherUsernameTest() throws Exception {
        Profile profile = new Profile();
        profile.setUsername("test");
        Mockito.when(profileService.findByUsername(any(String.class))).thenReturn(profile);
        this.mockMvc.perform(get("/users/{username}/profile", "username"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/users/test/profile"));
    }
}
