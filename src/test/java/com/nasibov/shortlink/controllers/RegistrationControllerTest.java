package com.nasibov.shortlink.controllers;


import com.nasibov.shortlink.configs.TestServicesConfig;
import com.nasibov.shortlink.models.User;
import com.nasibov.shortlink.services.registration.RegistrationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(RegistrationController.class)
@Import(TestServicesConfig.class)
public class RegistrationControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private RegistrationService registrationService;
    private User user;

    @Before
    public void setUp() {
        User user = new User();
        user.setUsername("testUsername");
        user.setFirstname("testFirstname");
        user.setLastname("testLastname");
        user.setEmail("test@ya.ru");
        user.setPassword("testPassword");
        this.user = user;
    }

    @Test
    public void getRegistrationPageTest() throws Exception {
        this.mockMvc.perform(get("/registration")).andExpect(status().isOk());
    }

    @Test
    public void registrationTest() throws Exception {
        Mockito.when(this.registrationService.registration(any(User.class))).thenReturn(ResponseEntity.status(HttpStatus.OK).body(this.user.getUsername()));
        this.mockMvc.perform(post("/registration")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(buildUrlEncodedFormEntity(
                        "firstname", this.user.getFirstname(),
                        "username", this.user.getLastname(),
                        "lastname", this.user.getLastname(),
                        "email", this.user.getEmail(),
                        "password", this.user.getPassword()
                )))
                .andExpect(status().isOk());


    }

    @Test
    public void registrationWithoutPasswordShouldFailTest() throws Exception {
        Mockito.when(this.registrationService.registration(any(User.class))).thenReturn(ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null));
        this.mockMvc.perform(post("/registration")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content(buildUrlEncodedFormEntity(
                        "firstname", this.user.getFirstname(),
                        "lastname", this.user.getLastname(),
                        "username", this.user.getUsername(),
                        "email", this.user.getEmail()
                )))
                .andExpect(status().isBadRequest());
    }

    private String buildUrlEncodedFormEntity(String... params) {
        if ((params.length % 2) > 0) {
            throw new IllegalArgumentException("Need to give an even number of parameters");
        }
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < params.length; i += 2) {
            if (i > 0) {
                result.append('&');
            }
            try {
                result.
                        append(URLEncoder.encode(params[i], StandardCharsets.UTF_8.name())).
                        append('=').
                        append(URLEncoder.encode(params[i + 1], StandardCharsets.UTF_8.name()));
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }
        return result.toString();
    }
}
