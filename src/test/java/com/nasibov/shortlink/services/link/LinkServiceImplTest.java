package com.nasibov.shortlink.services.link;

import com.nasibov.shortlink.models.Link;
import com.nasibov.shortlink.repositories.LinkRepository;
import com.nasibov.shortlink.services.link.implementations.LinkServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class LinkServiceImplTest {

    @Mock
    LinkRepository linkRepository;

    @InjectMocks
    LinkServiceImpl linkService;

    private List<Link> links;
    private Link link;
    private String baseLink = "baseLink";

    @Before
    public void setUp() {
        this.links = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Link link = new Link();
            String baseLink = "baseLink" + i;
            link.setOwnerUsername("username");
            link.setBaseLink(baseLink);
            this.links.add(link);
        }

        this.link = new Link();
        link.setOwnerUsername("test");
        link.setBaseLink("baseLink");

    }

    @Test
    public void getLinksByUsernameTest() {
        when(this.linkRepository.findAllByOwnerUsername(any(String.class))).thenReturn(this.links);
        List<Link> links = linkService.getAllByUsername("username");
        assertEquals(this.links, links);
        verify(this.linkRepository, times(1)).findAllByOwnerUsername(any(String.class));
    }

    @Test
    public void saveNewLinkTest() {
        when(this.linkRepository.save(any(Link.class))).thenReturn(this.link);
        Link link = linkService.saveNewLink(baseLink, "test");
        assertEquals(this.link, link);
        verify(this.linkRepository, times(1)).save(any(Link.class));
    }

    @Test
    public void getLinkByIdTest() {
        when(this.linkRepository.findById(any(String.class))).thenReturn(Optional.of(this.link));
        Link link = this.linkService.getLinkById("linkId");
        assertEquals(this.link, link);
        verify(this.linkRepository, times(1)).findById(any(String.class));
    }
}