package com.nasibov.shortlink.services.user;

import com.nasibov.shortlink.models.User;
import com.nasibov.shortlink.repositories.UserRepository;
import com.nasibov.shortlink.services.profile.ProfileService;
import com.nasibov.shortlink.services.user.implementations.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class UserServiceImplTest {

    @Mock
    UserRepository userRepository;

    @Mock
    ProfileService profileService;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void save() {
        User user = new User();
        user.setUsername("username");
        user.setPassword("password");
        when(userRepository.save(any(User.class))).thenReturn(user);
        User savedUser = userService.save(user);
        Assert.assertEquals(user.getUsername(), savedUser.getUsername());
        verify(profileService, times(1)).createProfileForUser(any(User.class));
    }

    @Test
    public void loadUserByUsername_shouldReturnUserDetails() {
        User user = new User();
        user.setUsername("username");
        when(userRepository.findByUsername("username")).thenReturn(Optional.of(user));
        UserDetails userDetails = userService.loadUserByUsername("username");
        Assert.assertEquals(user.getUsername(), userDetails.getUsername());
    }

    @Test(expected = UsernameNotFoundException.class)
    public void loadUserByUsername_shouldThrowUsernameNotFoundException() {
        when(userRepository.findByUsername(any(String.class))).thenReturn(Optional.empty());
        userService.loadUserByUsername("username");
    }
}
