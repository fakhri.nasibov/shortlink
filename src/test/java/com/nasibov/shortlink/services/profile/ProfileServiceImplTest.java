package com.nasibov.shortlink.services.profile;

import com.nasibov.shortlink.models.Profile;
import com.nasibov.shortlink.models.User;
import com.nasibov.shortlink.repositories.ProfileRepository;
import com.nasibov.shortlink.services.profile.implementations.ProfileServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
public class ProfileServiceImplTest {

    @Mock
    ProfileRepository profileRepository;

    @InjectMocks
    ProfileServiceImpl profileService;

    private User user;

    @Before
    public void setUp() {
        User user = new User();
        user.setUsername("username");
        this.user = user;
    }

    @Test
    public void createProfileForUserTest() {
        Profile profile = new Profile();
        profile.setUsername(this.user.getUsername());

        Mockito.when(profileRepository.save(any(Profile.class))).thenReturn(profile);
        Profile profileForUser = profileService.createProfileForUser(this.user);
        Assert.assertEquals(profileForUser.getUsername(), this.user.getUsername());
    }

    @Test
    public void findByUsernameTest() {
        Profile profile = new Profile();
        profile.setUsername(this.user.getUsername());
        Mockito.when(profileRepository.findByUsername(any(String.class))).thenReturn(Optional.of(profile));
        Optional<Profile> profileByUserId = profileRepository.findByUsername(this.user.getUsername());
        profileByUserId.ifPresent(p -> {
            Assert.assertEquals(profile.getUsername(), this.user.getUsername());
        });
    }

    @Test(expected = UsernameNotFoundException.class)
    public void finByUsername_ShouldThrowExceptionTest() {
        Mockito.when(profileRepository.findByUsername(any(String.class))).thenReturn(Optional.empty());
        profileService.findByUsername("not existing username");
    }
}
