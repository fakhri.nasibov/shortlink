package com.nasibov.shortlink.services.registration;

import com.nasibov.shortlink.models.User;
import com.nasibov.shortlink.services.registration.implementations.RegistrationServiceImpl;
import com.nasibov.shortlink.services.user.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class RegistrationServiceImplTest {

    @Mock
    UserService userService;

    @Mock
    PasswordEncoder passwordEncoder;

    @InjectMocks
    RegistrationServiceImpl registrationService;

    @Test
    public void registration_status_Ok() {
        User user = new User();
        user.setUsername("username");
        user.setPassword("password");
        ResponseEntity<String> registrationResponse = registrationService.registration(user);
        HttpStatus statusCode = registrationResponse.getStatusCode();
        String body = registrationResponse.getBody();
        Assert.assertEquals(statusCode, HttpStatus.OK);
        Assert.assertEquals(body, user.getUsername());
    }

    @Test
    public void registration_status_BadRequest() {
        User user = new User();
        user.setUsername("username");
        ResponseEntity<String> registrationResponse = registrationService.registration(user);
        HttpStatus statusCode = registrationResponse.getStatusCode();
        String body = registrationResponse.getBody();
        Assert.assertEquals(statusCode, HttpStatus.BAD_REQUEST);
        Assert.assertEquals(body, "Wrong password!");
    }

    @Test
    public void isPasswordValid_shouldReturnTrue() {
        boolean isValid = registrationService.isPasswordValid("password");
        Assert.assertTrue(isValid);
    }

    @Test
    public void isPasswordValid_shouldReturnFalse() {
        boolean isValid = registrationService.isPasswordValid(null);
        Assert.assertFalse(isValid);
    }
}
