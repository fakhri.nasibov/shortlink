package com.nasibov.shortlink.repositories;

import com.nasibov.shortlink.models.Link;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LinkRepository extends MongoRepository<Link, String> {
    List<Link> findAllByOwnerUsername(String username);
}
