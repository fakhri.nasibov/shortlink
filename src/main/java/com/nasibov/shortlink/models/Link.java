package com.nasibov.shortlink.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode
public class Link {
    @Id
    private String id;

    private String ownerUsername;

    private String baseLink;
}
