package com.nasibov.shortlink.services.user.implementations;

import com.nasibov.shortlink.models.User;
import com.nasibov.shortlink.repositories.UserRepository;
import com.nasibov.shortlink.services.profile.ProfileService;
import com.nasibov.shortlink.services.user.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ProfileService profileService;
    public UserServiceImpl(UserRepository userRepository, ProfileService profileService) {
        this.userRepository = userRepository;
        this.profileService = profileService;
    }

    @Override
    @Transactional
    public User save(User user) {
        user = userRepository.save(user);
        profileService.createProfileForUser(user);
        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username not found!"));
    }
}
