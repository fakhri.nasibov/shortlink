package com.nasibov.shortlink.services.profile.implementations;

import com.nasibov.shortlink.models.Profile;
import com.nasibov.shortlink.models.User;
import com.nasibov.shortlink.repositories.ProfileRepository;
import com.nasibov.shortlink.services.profile.ProfileService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class ProfileServiceImpl implements ProfileService {

    private final ProfileRepository profileRepository;

    public ProfileServiceImpl(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    @Override
    public Profile createProfileForUser(User user) {
        Profile profile = new Profile();
        profile.setUsername(user.getUsername());
        return profileRepository.save(profile);
    }

    @Override
    public Profile findByUsername(String username) {
        return profileRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username does not exist"));
    }

}
