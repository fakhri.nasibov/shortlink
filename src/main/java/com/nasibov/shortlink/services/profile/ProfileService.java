package com.nasibov.shortlink.services.profile;

import com.nasibov.shortlink.models.Profile;
import com.nasibov.shortlink.models.User;

import java.util.Optional;

public interface ProfileService {
    Profile createProfileForUser(User user);

    Profile findByUsername(String username);
}
