package com.nasibov.shortlink.services.link.implementations;

import com.nasibov.shortlink.models.Link;
import com.nasibov.shortlink.repositories.LinkRepository;
import com.nasibov.shortlink.services.link.LinkService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LinkServiceImpl implements LinkService {
    private final LinkRepository linkRepository;

    public LinkServiceImpl(LinkRepository linkRepository) {
        this.linkRepository = linkRepository;
    }

    @Override
    public List<Link> getAllByUsername(String username) {
        return this.linkRepository.findAllByOwnerUsername(username);
    }

    @Override
    public Link saveNewLink(String url, String username) {
        Link link = new Link();
        link.setBaseLink(url);
        link.setOwnerUsername(username);

        return this.linkRepository.save(link);
    }

    @Override
    public Link getLinkById(String id) {
        return this.linkRepository.findById(id).orElseThrow(() -> new RuntimeException("Link not found!"));
    }
}
