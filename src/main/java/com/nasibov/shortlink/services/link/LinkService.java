package com.nasibov.shortlink.services.link;


import com.nasibov.shortlink.models.Link;

import java.util.List;

public interface LinkService {
    List<Link> getAllByUsername(String username);

    Link saveNewLink(String url, String username);

    Link getLinkById(String id);
}
