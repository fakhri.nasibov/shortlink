package com.nasibov.shortlink.services.registration;

import com.nasibov.shortlink.models.User;
import org.springframework.http.ResponseEntity;

public interface RegistrationService {
    ResponseEntity<String> registration(User user);

    boolean isPasswordValid(String password);
}
