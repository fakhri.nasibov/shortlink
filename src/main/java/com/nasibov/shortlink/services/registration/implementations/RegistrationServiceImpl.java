package com.nasibov.shortlink.services.registration.implementations;

import com.nasibov.shortlink.models.Authorities;
import com.nasibov.shortlink.models.User;
import com.nasibov.shortlink.services.registration.RegistrationService;
import com.nasibov.shortlink.services.user.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class RegistrationServiceImpl implements RegistrationService {
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    public RegistrationServiceImpl(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public ResponseEntity<String> registration(User user) {
        if (!isPasswordValid(user.getPassword())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Wrong password!");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setAuthorities(Collections.singleton(Authorities.ROLE_USER));
        userService.save(user);
        return ResponseEntity.status(HttpStatus.OK).body(user.getUsername());
    }

    @Override
    public boolean isPasswordValid(String password) {
        return password != null;
    }
}
