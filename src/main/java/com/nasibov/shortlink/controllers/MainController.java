package com.nasibov.shortlink.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class MainController {

    @GetMapping({"/main", "/index"})
    public void redirectToIndex(HttpServletResponse response) throws IOException {
        response.sendRedirect("/");
    }

    @GetMapping("/")
    public String index() {
        return "index";
    }
}
