package com.nasibov.shortlink.controllers;

import com.nasibov.shortlink.models.Link;
import com.nasibov.shortlink.services.link.LinkService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/links")
public class LinkController {

    private final LinkService linkService;

    public LinkController(LinkService linkService) {
        this.linkService = linkService;
    }

    @GetMapping
    public @ResponseBody
    List<Link> getLinksByUsername(Principal principal) {

        return this.linkService.getAllByUsername(principal.getName());
    }

    @PostMapping
    public @ResponseBody
    Link createNewLink(Principal principal, @RequestParam("url") String url) {
        return this.linkService.saveNewLink(url, principal.getName());
    }

    @GetMapping("/{id}")
    public void redirectToLinkBaseUrl(HttpServletResponse response, @PathVariable String id) throws IOException {
        Link linkById = linkService.getLinkById(id);
        response.sendRedirect(linkById.getBaseLink());
    }
}
