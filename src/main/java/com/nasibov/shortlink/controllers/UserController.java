package com.nasibov.shortlink.controllers;

import com.nasibov.shortlink.models.Profile;
import com.nasibov.shortlink.services.link.LinkService;
import com.nasibov.shortlink.services.profile.ProfileService;
import com.nasibov.shortlink.services.user.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;

@Controller
@RequestMapping("/users")
public class UserController {

    private final ProfileService profileService;
    private final UserService userService;
    private final LinkService linkService;

    public UserController(ProfileService profileService, UserService userService, LinkService linkService) {
        this.profileService = profileService;
        this.userService = userService;
        this.linkService = linkService;
    }

    @GetMapping("/{username}/profile")
    public String getUserProfilePage(Principal principal, HttpServletResponse response, @PathVariable String username, Model model) throws IOException {
        String principalUsername = principal.getName();
        if (!username.equals(principalUsername)) {
            response.sendRedirect("/users/" + principalUsername + "/profile");
        }
        Profile profile = profileService.findByUsername(principalUsername);
        model.addAttribute("profile", profile);
        model.addAttribute("links", linkService.getAllByUsername(principalUsername));

        return "profile";
    }
}
